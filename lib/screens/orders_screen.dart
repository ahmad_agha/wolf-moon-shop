import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/widgets/orders_item.dart';
import '../providers/orders.dart' show Orders;
import '../widgets/app_drawer.dart';

class OrdersScreen extends StatelessWidget {
  static const routName = '/orders';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Orders'),
        centerTitle: true,
      ),
      drawer: AppDrawer(),
      body: FutureBuilder(
        future: Provider.of<Orders>(context,listen: false).fetchAndSetProduct(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator(),);
          } else {
            if (snapshot.error != null) {
              return Center(child: Text('An error occurred!'),);
            } else {
              return Consumer<Orders>(
                builder: (context, ordersData, child) =>
                    ListView.builder(
                      itemCount: ordersData.orders.length,
                      itemBuilder: (context, index) =>OrderItem(ordersData.orders[index]),
                    ),
              );
            }
          }
        },
      ),
    );
  }
}

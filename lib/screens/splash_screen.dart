import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../widgets/hex_color.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('#e3e3e3'),
      body: SpinKitFoldingCube(
        color: HexColor('#f95959'),
      ),
    );
  }
}



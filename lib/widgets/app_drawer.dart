import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/auth.dart';
import '../screens/orders_screen.dart';
import '../screens/user_products_screen.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final deviceSize= MediaQuery.of(context).size;
    return deviceSize.width<=600 ?
    SafeArea(
      child: Container(
        width: deviceSize.width*0.83,
        child: CustomDrawer(),
      ),
    ):
    CustomDrawer()
    ;
  }
}
class CustomDrawer extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return  Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            AppBar(
              title: Text('hello'),
              automaticallyImplyLeading: false,
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.shop),
              title: Text('Shop'),
              onTap: () => Navigator.of(context).pushReplacementNamed('/'),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.payment),
              title: Text('Orders'),
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(OrdersScreen.routName),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.edit),
              title: Text('Mange product'),
              onTap: () => Navigator.of(context)
                  .pushReplacementNamed(UserProductScreen.routName),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: ListTile(
                leading: Icon(Icons.logout),
                title: Text('Logout'),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacementNamed('/');
                  Provider.of<Auth>(context, listen: false).logout();
                },
              ),
            )
          ],
        ),
      ),
    );

  }
}
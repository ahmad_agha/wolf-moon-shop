import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../providers/cart.dart';

class OrderItem {
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItem({
    @required this.id,
    @required this.amount,
    @required this.products,
    @required this.dateTime,
  });
}

class Orders with ChangeNotifier {
  List<OrderItem> _orders = [];
  String authToken;
  String userId;

  getData(String authToken, String uId, List<OrderItem> orders) {
    this.authToken = authToken;
    this.userId = uId;
    this._orders = orders;
    notifyListeners();
  }

  List<OrderItem> get orders {
    return [..._orders];
  }

  Future<void> fetchAndSetProduct() async {
    final url = Uri.parse(
        'https://shop-app-86723-default-rtdb.firebaseio.com/orders/$userId.json?auth=$authToken');
    try {
      final res = await http.get(url);
      final extractedData = jsonDecode(res.body) as Map<String, dynamic>;
      if (extractedData == null) {
        return;
      }
      final List<OrderItem> loadedOrders = [];
      extractedData.forEach((orderId, orderData) {
        loadedOrders.add(
          OrderItem(
              id: orderId,
              amount: orderData['amount'],
              dateTime: DateTime.parse(orderData['dateTime']),
              products: (orderData['products'] as List<dynamic>)
                  .map((item) => CartItem(
                        id: item['id'],
                        title: item['title'],
                        quantity: item['quantity'],
                        price: item['price'],
                      ))
                  .toList()),
        );
      });
      _orders = loadedOrders.reversed.toList();
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }

  Future<void> addOrder(List<CartItem> cartProduct, double total) async {
    final url = Uri.parse(
        'https://shop-app-86723-default-rtdb.firebaseio.com/orders/$userId.json?auth=$authToken');
    try {
      final timestamp = DateTime.now();
      final res = await http.post(url,
          body: jsonEncode({
            'amount': total,
            'dateTime': timestamp.toString(),
            'products': cartProduct
                .map((cp) => {
                      'id': cp.id,
                      'title': cp.title,
                      'quantity': cp.quantity,
                      'price': cp.price,
                    })
                .toList(),
          }));
      _orders.insert(
          0,
          OrderItem(
            id: jsonDecode(res.body)['name'],
            amount: total,
            products: cartProduct,
            dateTime: timestamp,
          ));
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }
}

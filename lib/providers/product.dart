import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    this.isFavorite = false,
  });

  void _setFavValue(bool newValue) {
    isFavorite = newValue;
  }

  Future<void> toggleFavoriteStatus(String token, String userId) async {
    final _oldStatus = isFavorite;
    isFavorite = !isFavorite;

    final url = Uri.parse(
        'https://shop-app-86723-default-rtdb.firebaseio.com/userFavorites/$userId/$id.json?auth=$token');
    try {
      final res = await http.put(url, body: jsonEncode(isFavorite));
      if (res.statusCode >= 400) {
        _setFavValue(_oldStatus);
      }
    } catch (e) {}
    notifyListeners();
  }
}
